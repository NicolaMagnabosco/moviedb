import React from "react";
import ReactDOM from "react-dom";
import App from "./App.jsx";
import NetworkService from "./services/NetworkService";
import MovieDbService from "./services/MovieDbService";

let movieDbService = new MovieDbService(new NetworkService())

ReactDOM.render(<App movieDbService={movieDbService}/>, document.getElementById("root"));