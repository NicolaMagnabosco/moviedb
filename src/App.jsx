import React, { Component} from "react";
import { hot } from "react-hot-loader";
import ContentBrowser from "./components/ContentBrowser";
import "./styles/App.scss";

export class App extends Component{

  render() {
    return(
      <div className="app">
        <h1>The Movie DB</h1>
        <h3>Search for your favourite movie tv and shows</h3>
        <ContentBrowser movieDbService={this.props.movieDbService}/>
      </div>);
  }

}

export default hot(module)(App);