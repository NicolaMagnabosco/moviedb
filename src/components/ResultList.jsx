import React, { Component} from "react";
import SearchResult from "./SearchResult";
import "./../styles/ResultList.scss";

export default class ResultList extends Component{

  render() {
    return(<ul className="result-list">
      {this.props.items.map((item) => <SearchResult key={item.id} item={item}/>)}
    </ul>);
  }
}

