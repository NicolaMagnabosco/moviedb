import React, { Component} from "react";
import "./../styles/SearchBar.scss";

export default class SearchBar extends Component{

  constructor(props) {
    super(props);
    this.searchInputRef = React.createRef();
  }

  render(){
    return(
      <div className="search-bar">
        <input
          ref={this.searchInputRef}
          placeholder="Search for a movie"
          value={this.props.searchTerm}
          onChange={this.handleOnChange.bind(this)}/>
        <button onClick={this.handleOnClearClick.bind(this)}>Clear</button>
      </div>
    );
  }

  componentDidMount() {
    this.searchInputRef.current.focus();
  }

  handleOnChange(e) {
    this.props.onSearchTermChanged(e.target.value);
  }

  handleOnClearClick() {
    this.searchInputRef.current.value = "";
    this.props.onSearchTermChanged("");
  }
}

