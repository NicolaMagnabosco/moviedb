import React, { Component} from "react";
import SearchBar from "./SearchBar";
import ResultList from "./ResultList";
import { SearchType } from '../services/MovieDbService';
import LoadingWrapper from './LoadingWrapper';
import "./../styles/ContentBrowser.scss";
import SearchTypeSelector from "./SearchTypeSelector";

export default class ContentBrowser extends Component{

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      searchType: SearchType.ALL,
      isSearching: false,
      currentSearchTerm: ""
    };
  }

  render(){
    return(
      <div className="content-browser">
        <div className="search-container">
          <SearchBar searchTerm={this.currentSearchTerm} onSearchTermChanged={this.handleOnSearchTermChanged.bind(this)}/>
          <SearchTypeSelector selectedType={this.state.searchType} onTypeSelected={this.handleOnTypeSelected.bind(this)}/>
        </div>
        <LoadingWrapper loading={this.state.isSearching}>
          <ResultList items={this.state.items}/>
        </LoadingWrapper>
      </div>
    );
  }

  handleOnTypeSelected(type) {
    this.setState({ searchType: type });
    this.performSearch(type, this.state.currentSearchTerm);
  }

  handleOnSearchTermChanged(searchTerm) {
    this.setState({ currentSearchTerm: searchTerm });
    this.performSearch(this.state.searchType, searchTerm);
  }

  performSearch(searchType, searchTerm) {
    this.setState({ isSearching: true })
    this.props.movieDbService.search(searchType, searchTerm)
      .then((movies) => this.setState({ items: movies, isSearching: false }));
  }
}

