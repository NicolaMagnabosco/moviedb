import React, { Component} from "react";
import "./../styles/SearchTypeSelector.scss";
import { SearchType } from "./../services/MovieDbService";

export default class SearchTypeSelector extends Component{

  constructor(props) {
    super(props);
    this.searchType = React.createRef();
    this.searchTypeOptions = [
      { key: SearchType.ALL, translation: 'All' },
      { key: SearchType.MOVIE, translation: 'Movie' },
      { key: SearchType.TV_SHOWS, translation: 'Tv Series' }
    ];
  }

  render() {
    return(
        <div className="search-type-selector">
          {this.rendersOptions()}
        </div>
    );
  }

  rendersOptions() {
    return this.searchTypeOptions.map((option) => {
      return (
        <label key={option.key}>
          <input
            type="radio"
            name="search-type"
            value={option.key}
            checked={option.key === this.props.selectedType}
            onChange={this.handleOnChange.bind(this)}/>
          {option.translation}
        </label>
      );
    });
  }

  handleOnChange(e) {
    this.props.onTypeSelected(e.target.value);
  }

}

