import React, { Component} from "react";
import "./../styles/LoadingWrapper.scss";

export default class LoadingWrapper extends Component{

  render(){
    let computedClassName = `loading-wrapper ${this.props.loading ? "loading" : "" }`;
    return(<div className={computedClassName}>
      <div className="loading-indicator"/>
      {this.props.loading ? null : this.props.children}
    </div>);
  }
}

