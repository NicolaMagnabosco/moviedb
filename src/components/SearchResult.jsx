import React, { Component} from "react";
import "./../styles/SearchResult.scss";
const IMAGE_PATH_PREFIX = "https://image.tmdb.org/t/p/w500";
export default class SearchList extends Component{

  render() {
    let { poster_path, title } = this.props.item;
    return(<li className="search-result">
      {poster_path ? <img src={`${IMAGE_PATH_PREFIX}${poster_path}`}></img> : null}
      <span>{title}</span>
    </li>);
  }
}

