import { MOVIEDB_API_KEY, MOVIEDB_API_URL } from "./../config";

export const SearchType = {
  ALL: 'multi',
  MOVIE: 'movie',
  TV_SHOWS: 'tv'
};

/**
 * A service for communicating with themoviedb API
 */
export default class MovieDbService {

  constructor(networkService) {
    this.networkService = networkService;
  }

  /**
   * Creates a guest session.
   */
  authenticateGuestUser() {
    return this.networkService.get(`${MOVIEDB_API_URL}/authentication/guest_session/new?api_key=${MOVIEDB_API_KEY}`)
    .then((result) => {
      this.guestSessionId = result.guest_session_id;
      return Promise.resolve(this.guestSessionId);
    })
  }

  /**
   * Performs a search on the movie db.
   * @param {SearchType} type
   * @param {String} searchTerm
   * @param {Number} page
   */
  search(type, searchTerm = "", page = 1) {
    if (!type || !searchTerm) {
      return Promise.resolve([]);
    }
    return this.networkService.get(`${MOVIEDB_API_URL}/search/${type}?api_key=${MOVIEDB_API_KEY}&query=${searchTerm.trim()}&page=${page}&language=en-US`)
      .then((result) => Promise.resolve(result.results));
  }

}