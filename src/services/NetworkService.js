
export default class NetworkService {

  /**
   * Sends a GET request for the given URL.
   * @param {String} url The URL of the request.
   */
  get(url) {
   return fetch(url)
   .then((result) => {
     if (result.status === 200) {
       return result.json();
     } else {
       throw new Error(`Cannot get the given resource with url ${url}`);
     }
   })
  }

  /**
   * Sends a POST request.
   * @param {String} url The URL of the request.
   * @param {Object} body The body of the request.
   */
  post(url, body) {
    // todos
  }

}