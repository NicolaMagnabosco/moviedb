import chai from 'chai'
import React from 'react'
import ContentBrowser from '../src/components/ContentBrowser'
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SearchBar from '../src/components/SearchBar';
import SearchTypeSelector from '../src/components/SearchTypeSelector';
import LoadingWrapper from '../src/components/LoadingWrapper';
import ResultList from '../src/components/ResultList';

import sinon from 'sinon';
import { SearchType } from '../src/services/MovieDbService';

configure({ adapter: new Adapter() });
let expect = chai.expect;

describe("spec Content browser tests", () => {

    it('renders search container with search wrapper, search bar', ()=> {
      let mockMovieDbService = {
        search: () => Promise.resolve([])
      };

      const wrapper = shallow(<ContentBrowser movieDbService={mockMovieDbService}/>);
      expect(wrapper.find('.content-browser')).to.have.length(1);
      expect(wrapper.find(SearchBar)).to.have.length(1);
    });

    it('renders search type selector', ()=> {
      let mockMovieDbService = {
        search: () => Promise.resolve([])
      };

      const wrapper = shallow(<ContentBrowser movieDbService={mockMovieDbService}/>);
      expect(wrapper.find(SearchTypeSelector)).to.have.length(1);
    });

    it('renders loading wrapper with result list', ()=> {
      let mockMovieDbService = {
        search: () => Promise.resolve([])
      };

      const wrapper = shallow(<ContentBrowser movieDbService={mockMovieDbService}/>);
      expect(wrapper.find(LoadingWrapper)).to.have.length(1);
      expect(wrapper.find(ResultList)).to.have.length(1);
    });

    it('call search on mock service when search term changes', ()=> {
      let searchSpy = sinon.stub();
      searchSpy.returns(Promise.resolve([]));

      let mockMovieDbService = {
        search: searchSpy
      };

      const wrapper = shallow(<ContentBrowser movieDbService={mockMovieDbService}/>);
      wrapper.find(SearchBar).props().onSearchTermChanged("dummy search");

      expect(searchSpy.called).to.be.true;
      expect(searchSpy.calledWith(SearchType.ALL, "dummy search")).to.be.true;

    });

    it('call search on mock service when search type changes', ()=> {
      let searchSpy = sinon.stub();
      searchSpy.returns(Promise.resolve([]));

      let mockMovieDbService = {
        search: searchSpy
      };

      const wrapper = shallow(<ContentBrowser movieDbService={mockMovieDbService}/>);
      wrapper.find(SearchTypeSelector).props().onTypeSelected(SearchType.MOVIE);

      expect(searchSpy.called).to.be.true;
      expect(searchSpy.calledWith(SearchType.MOVIE, "")).to.be.true;

    });

})