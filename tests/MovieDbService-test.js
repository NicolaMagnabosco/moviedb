import chai from 'chai';
import sinon from 'sinon';
import MovieDbService, { SearchType } from './../src/services/MovieDbService';
import { MOVIEDB_API_KEY } from '../src/config';
let expect = chai.expect;

describe("MovieDbService test", () => {
  it('send get request using network service', (done) => {
    let getStub = sinon.stub().returns(Promise.resolve({
      guest_session_id: "dummyId"
    }));
    let mockNetworkService = {
      get: getStub
    };

    let movieDbService = new MovieDbService(mockNetworkService);
    movieDbService.authenticateGuestUser().then((sessionId) => {
      expect(getStub.called).to.be.true;
      expect(sessionId).to.be.eq("dummyId");
      done();
    });
  });

  it('search sends get with given search term and search type', (done) => {
    let getStub = sinon.stub().returns(Promise.resolve({
      results: []
    }));
    let mockNetworkService = {
      get: getStub
    };

    let movieDbService = new MovieDbService(mockNetworkService);
    movieDbService.search("movie", "dummyTerm", 3).then(() => {
      expect(getStub.called).to.be.true;

      expect(getStub.lastCall.args[0]).to.contain(`api_key=${MOVIEDB_API_KEY}`);
      expect(getStub.lastCall.args[0]).to.contain("movie");
      expect(getStub.lastCall.args[0]).to.contain("query=dummyTerm");
      expect(getStub.lastCall.args[0]).to.contain("page=3");
      done();
    });
  });

  it('searching returns empty results if no type is provided', (done) => {
    let getStub = sinon.stub().returns(Promise.resolve({
      results: []
    }));
    let mockNetworkService = {
      get: getStub
    };

    let movieDbService = new MovieDbService(mockNetworkService);
    movieDbService.search().then((results) => {
      expect(results).to.be.deep.eq([]);
      done();
    });
  });

  it('searching returns empty results if no search term is provided', (done) => {
    let getStub = sinon.stub().returns(Promise.resolve({
      results: []
    }));
    let mockNetworkService = {
      get: getStub
    };

    let movieDbService = new MovieDbService(mockNetworkService);
    movieDbService.search("MOVIE").then((results) => {
      expect(results).to.be.deep.eq([]);
      done();
    });
  });

  it('searching returns results from search', (done) => {
    let dummyResult1 = { id: 1 };
    let dummyResult2 = { id: 2 };
    let getStub = sinon.stub().returns(Promise.resolve({
      results: [ dummyResult1, dummyResult2 ]
    }));
    let mockNetworkService = {
      get: getStub
    };

    let movieDbService = new MovieDbService(mockNetworkService);
    movieDbService.search("MOVIE", "dummyTerm").then((results) => {
      expect(results).to.be.deep.eq([ dummyResult1, dummyResult2 ]);
      done();
    });
  });
})