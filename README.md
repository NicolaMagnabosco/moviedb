# MOVIEDB: a simple React app for browsing www.themoviedb.org.

Description
----
A simple React application for fetching content from The Movie Db.

Stack:
-----
 - node v8.12.0, npm 6.4.1
 - babel 7
 - webpack 3
 - react 16
 - sass
 - chai, mocha and enzyme for testing

How to run the app
----
To run the app in your local environment run the following command:
```
  npm i && npm start
```

To run the tests:
```
  npm test
```

The app would be then be served on the following URL: http://localhost:3000

About the app
------
I have implemented a basic search functionality to fetch content by type and search term.

 - `NetworkService` is used to send HTTP requests. This can be easily reused by different services.

 - `MovieDbService` is responsible of fetching content from the movie db API. I have also implemented a basic authorisation method that allows user to login using a guest session.

 - `ContentBrowser` is the component responsible for rendering the search components and the search results. It also mantains the state of the search and communicates with the `MovieDbService` to perform the serach.

 - `SearchBar`: a simple search bar.
 - `SearchTypeSelector`: radio buttons for selecting the search type.
 - `ResultList`: renders the results of a search.
 - `SearchResult`: renders a single result item.
 - `LoadingWrapper`: a reusable component for rendering a loading animation.

As per requirements I did not use a state manager framework, thus the main state of the search is handled by the `ContentBrowser` component who is given a `MovieDbService`.

Testing
----
I have added unit testing for the `MovieDbService` and component tests for `ContentBrowser`. Given the time limit I did not add any other component tests but I would add a test for each components to check if it actually renders correctly given the injectet props and the callbacks are called.

